﻿using System;
using System.Threading;
using UnityEngine;

namespace Apache.Utilities {
	public class TextureUtilities {
		//-----------------------------------------------------------------------------------------
		// Class Declarations:
		//-----------------------------------------------------------------------------------------

		public class ThreadData {
			public int End;
			public int Start;

			public ThreadData(int s, int e) {
				Start = s;
				End = e;
			}
		}

		private class ScaleSettings {
			public Color[] TexColors;
			public Color[] NewColors;
			public int OldWidth;
			public float RatioX;
			public float RatioY;
			public int NewWidth;
			public int FinishCount;
			public Mutex Mutex;
		}

		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		public static Texture2D GenerateTexture2DFromRenderTexture(RenderTexture renderTexture) {
			Texture2D returnTex = new Texture2D(renderTexture.width, renderTexture.height);
			RenderTexture prevTex = RenderTexture.active;
			RenderTexture.active = renderTexture;
			returnTex.ReadPixels(new Rect(0, 0, returnTex.width, returnTex.height), 0, 0);
			returnTex.Apply();
			RenderTexture.active = prevTex;
			return returnTex;
		}

		public static void DrawOnGUILayoutSprite(Sprite sprite, int scale) {
			Rect c = sprite.rect;
			float spriteW = c.width;
			float spriteH = c.height;

			Rect rect = GUILayoutUtility.GetRect(spriteW * scale, spriteH * scale);
			if (Event.current.type == EventType.Repaint) {
				var tex = sprite.texture;
				c.xMin /= (tex.width);
				c.xMax /= (tex.width);
				c.yMin /= (tex.height);
				c.yMax /= (tex.height);
				GUI.DrawTextureWithTexCoords(rect, tex, c);
			}
		}

		public static void DrawOnGUISprite(Rect rect, Sprite sprite) {
			Rect c = sprite.rect;
			var tex = sprite.texture;
			c.xMin /= (tex.width);
			c.xMax /= (tex.width);
			c.yMin /= (tex.height);
			c.yMax /= (tex.height);
			GUI.DrawTextureWithTexCoords(rect, tex, c);
		}

		public static Texture2D GenerateTextureFromSprite(Sprite aSprite) {
			if (!aSprite || !aSprite.texture.IsReadable()) return null;

			Rect rect = aSprite.rect;
			Texture2D tex = new Texture2D((int) rect.width, (int) rect.height, TextureFormat.ARGB32, false);
			Color[] data = aSprite.texture.GetPixels((int) rect.x, (int) rect.y, (int) rect.width, (int) rect.height);
			tex.SetPixels(data);
			tex.filterMode = FilterMode.Point;
			tex.Apply(true);
			return tex;
		}

		public static void Point(Texture2D tex, int newWidth, int newHeight) {
			if (!tex.IsReadable()) return;

			ThreadedScale(tex, newWidth, newHeight, false);
		}

		public static void Bilinear(Texture2D tex, int newWidth, int newHeight) {
			if (!tex.IsReadable()) return;

			ThreadedScale(tex, newWidth, newHeight, true);
		}

		private static void BilinearScale(object obj, ScaleSettings settings) {
			ThreadData threadData = (ThreadData) obj;
			for (int y = threadData.Start; y < threadData.End; y++) {
				int yFloor = (int) Mathf.Floor(y * settings.RatioY);
				int y1 = yFloor * settings.OldWidth;
				int y2 = (yFloor + 1) * settings.OldWidth;
				int yw = y * settings.NewWidth;

				for (int x = 0; x < settings.NewWidth; x++) {
					int xFloor = (int) Mathf.Floor(x * settings.RatioX);
					float xLerp = x * settings.RatioX - xFloor;
					settings.NewColors[yw + x] = ColorExtensions.ULerp(ColorExtensions.ULerp(settings.TexColors[y1 + xFloor], settings.TexColors[y1 + xFloor + 1], xLerp),
						ColorExtensions.ULerp(settings.TexColors[y2 + xFloor], settings.TexColors[y2 + xFloor + 1], xLerp),
						y * settings.RatioY - yFloor);
				}
			}

			settings.Mutex.WaitOne();
			settings.FinishCount++;
			settings.Mutex.ReleaseMutex();
		}

		private static void PointScale(object obj, ScaleSettings settings) {
			ThreadData threadData = (ThreadData) obj;
			for (int y = threadData.Start; y < threadData.End; y++) {
				int thisY = (int) (settings.RatioY * y) * settings.OldWidth;
				int yw = y * settings.NewWidth;
				for (int x = 0; x < settings.NewWidth; x++) {
					settings.NewColors[yw + x] = settings.TexColors[(int) (thisY + settings.RatioX * x)];
				}
			}

			settings.Mutex.WaitOne();
			settings.FinishCount++;
			settings.Mutex.ReleaseMutex();
		}

		public static Texture2D CloneTexture(Texture2D texture) {
			if (texture == null || !texture.IsReadable()) return null;

			TextureFormat newFormat = GetValidFormat(texture.format);
			Texture2D newTexture = new Texture2D(texture.width, texture.height, newFormat, false);
			newTexture.SetPixels(texture.GetPixels());
			newTexture.filterMode = texture.filterMode;
			newTexture.Apply();
			return newTexture;
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private static void ThreadedScale(Texture2D tex, int newWidth, int newHeight, bool useBilinear) {
			if (!tex.IsReadable()) return;

			int cores = Mathf.Min(SystemInfo.processorCount, newHeight);
			int slice = newHeight / cores;

			ScaleSettings settings = new ScaleSettings() {
				FinishCount = 0,
				Mutex = new Mutex(false),
				NewColors = new Color[newWidth * newHeight],
				RatioX = useBilinear ? 1.0f / ((float) newWidth / (tex.width - 1)) : ((float) tex.width) / newWidth,
				RatioY = useBilinear ? 1.0f / ((float) newHeight / (tex.height - 1)) : ((float) tex.height) / newHeight,
				TexColors = tex.GetPixels(),
				OldWidth = tex.width,
				NewWidth = newWidth
			};

			Action<object> bilinearScale = delegate (object o) {
				BilinearScale(o, settings);
			};
			Action<object> pointScale = delegate (object o) {
				PointScale(o, settings);
			};

			if (cores > 1) {
				int i;
				ThreadData threadData;

				for (i = 0; i < cores - 1; i++) {
					threadData = new ThreadData(slice * i, slice * (i + 1));
					ParameterizedThreadStart ts = useBilinear ? new ParameterizedThreadStart(bilinearScale) : new ParameterizedThreadStart(pointScale);
					Thread thread = new Thread(ts);
					thread.Start(threadData);
				}
				threadData = new ThreadData(slice * i, newHeight);
				if (useBilinear) {
					bilinearScale(threadData);
				}
				else {
					pointScale(threadData);
				}
				while (settings.FinishCount < cores) {
					Thread.Sleep(1);
				}
			}
			else {
				ThreadData threadData = new ThreadData(0, newHeight);
				if (useBilinear) {
					bilinearScale(threadData);
				}
				else {
					pointScale(threadData);
				}
			}

			tex.Resize(newWidth, newHeight);
			tex.SetPixels(settings.NewColors);
			tex.Apply();
		}

		private static TextureFormat GetValidFormat(TextureFormat format) {
			if (format != TextureFormat.ARGB32 && format != TextureFormat.RGBA32 && format != TextureFormat.RGB24 && format != TextureFormat.Alpha8 && format != TextureFormat.RFloat &&
				format != TextureFormat.RGBAFloat && format != TextureFormat.RGFloat) {
				return TextureFormat.RGBA32;
			}
			return format;
		}
	}
}