using System;
using System.Globalization;
using System.Text;

namespace Apache.Utilities {
	public static class StringExtensions {
		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		/// <summary>
		/// Limits an 'input' string length to 'charLimit'. 
		/// When 'input'.Lenght > 'charlimit', '...' will be the last 3 characters.
		/// </summary>
		public static string SuspensionClip(this string input, int charLimit) {
			if (input.Length <= charLimit)
				return input;

			string output = "";
			for (int i = 0; i < charLimit - 3; i++) {
				if (input.Length > i) {
					output += input[i];
				}
			}
			output += "...";

			return output;
		}

		/// <summary>
		/// Eg MY_INT_VALUE => MyIntValue
		/// </summary>
		public static string ToTitleCase(this string input) {
			var builder = new StringBuilder();
			for (int i = 0; i < input.Length; i++) {
				var current = input[i];
				if (current == '_' && i + 1 < input.Length) {
					var next = input[i + 1];
					if (char.IsLower(next)) {
						next = char.ToUpper(next, CultureInfo.InvariantCulture);
					}

					builder.Append(next);
					i++;
				}
				else {
					builder.Append(current);
				}
			}

			return builder.ToString();
		}

		/// <summary>
		/// Returns whether or not the specified string is contained with this string
		/// </summary>
		public static bool Contains(this string source, string toCheck, StringComparison comparisonType) {
			return source.IndexOf(toCheck, comparisonType) >= 0;
		}

		/// <summary>
		/// Ex: "thisIsCamelCase" -> "This Is Camel Case"
		/// </summary>
		public static string SplitPascalCase(this string input) {
			StringBuilder sb = new StringBuilder(input.Length);

			sb.Append(char.ToUpper(input[0]));

			for (int i = 1; i < input.Length; i++) {
				char c = input[i];

				if (char.IsUpper(c)) {
					sb.Append(' ');
				}

				sb.Append(c);
			}

			return sb.ToString();
		}

		/// <summary>
		/// Returns true if this string is null, empty, or contains only whitespace.
		/// </summary>
		/// <param name="str">The string to check.</param>
		/// <returns><c>true</c> if this string is null, empty, or contains only whitespace; otherwise, <c>false</c>.</returns>
		public static bool IsNullOrWhitespace(this string str) {
			if (!string.IsNullOrEmpty(str)) {
				for (int i = 0; i < str.Length; i++) {
					if (char.IsWhiteSpace(str[i]) == false) {
						return false;
					}
				}
			}

			return true;
		}
	}
}