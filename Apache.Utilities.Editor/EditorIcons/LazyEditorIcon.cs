using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace Apache.Utilities.Editor {
	public class LazyEditorIcon : EditorIcon {
		//-----------------------------------------------------------------------------------------
		// Constants:
		//-----------------------------------------------------------------------------------------

		private const string ICONS_PATH = "Assets/Content/Icons/Icons.png";

		private static readonly string ICON_SHADER = @"
Shader ""Hidden/Sirenix/Editor/GUIIcon""
{
	Properties
	{
        _MainTex(""Texture"", 2D) = ""white"" {}
        _Color(""Color"", Color) = (1,1,1,1)
        _Rect(""Rect"", Vector) = (0,0,0,0)
        _TexelSize(""TexelSize"", Vector) = (0,0,0,0)
	}
    SubShader
	{
		Tags { ""RenderType"" = ""Opaque"" }
        Blend SrcAlpha Zero
        Pass
        {
            CGPROGRAM
                " + /*Blame the old Unity compiler*/ "#" + @"pragma vertex vert
                " + "#" + @"pragma fragment frag
                " + "#" + @"include ""UnityCG.cginc""

                struct appdata
                {
                    float4 vertex : POSITION;
					float2 uv : TEXCOORD0;
				};

                struct v2f
                {
                    float2 uv : TEXCOORD0;
					float4 vertex : SV_POSITION;
				};

                sampler2D _MainTex;
                float4 _Rect;
                float4 _Color;
                float2 _TexelSize;

                v2f vert(appdata v)
                {
                    v2f o;
                    o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
                    o.uv = v.uv;
                    return o;
                }

                float SampleTexture(float2 uv)
                {
                    float3 texel = float3(_TexelSize.x, _TexelSize.y, 0);
                    return tex2D(_MainTex, uv + texel.zz).a;
                }

                fixed4 frag(v2f i) : SV_Target
				{
					fixed4 col = _Color;
                    float2 uv = i.uv;
                    uv *= _Rect.zw;
					uv += _Rect.xy;
					col.a *= SampleTexture(uv);
					return col;
				}
			ENDCG
		}
	}
}
";

		[NonSerialized] private static readonly Dictionary<string, Sprite> SPRITES = AssetDatabase.LoadAllAssetsAtPath(ICONS_PATH)
																								.OfType<Sprite>()
																								.ToDictionary(key => key.name.ToLower(), value => value);

		private static readonly Color HIGHLIGHTED_COLOR = new Color(0f, 0f, 0f, 0.8f);
		private static readonly Color HIGHLIGHTED_COLOR_PRO = new Color(1f, 1f, 1f, 1f);
		private static readonly Color INACTIVE_COLOR = new Color(0f, 0f, 0f, 0.57f);
		private static readonly Color INACTIVE_COLOR_PRO = new Color(0.6f, 0.6f, 0.6f, 1f);
		private static readonly Color ACTIVE_COLOR = new Color(0f, 0f, 0f, 0.6f);
		private static readonly Color ACTIVE_COLOR_PRO = new Color(0.6f, 0.6f, 0.6f, 1f);
		private static readonly bool IS_PRO = EditorGUIUtility.isProSkin;

		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private static Material iconMat;

		private readonly Sprite icon;
		private Texture highlighted;
		private Texture active;
		private Texture inactive;
		private readonly string iconName;

		//-----------------------------------------------------------------------------------------
		// Public Properties:
		//-----------------------------------------------------------------------------------------
		
		/// <summary>
		/// Gets the icon's highlight texture.
		/// </summary>
		public override Texture Highlighted { get { return highlighted ?? (highlighted = RenderIcon(IS_PRO ? HIGHLIGHTED_COLOR_PRO : HIGHLIGHTED_COLOR)); } }

		/// <summary>
		/// Gets the icon's active texture.
		/// </summary>
		public override Texture Active { get { return active ?? (active = RenderIcon(IS_PRO ? ACTIVE_COLOR_PRO : ACTIVE_COLOR)); } }

		/// <summary>
		/// Gets the icon's inactive texture.
		/// </summary>
		public override Texture Inactive { get { return inactive ?? (inactive = RenderIcon(IS_PRO ? INACTIVE_COLOR_PRO : INACTIVE_COLOR)); } }

		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		static LazyEditorIcon() {}

		/// <summary>
		/// Loads an EditorIcon from the spritesheet.
		/// </summary>
		public LazyEditorIcon(string icon) {
			this.icon = SPRITES[icon];
			iconName = icon;
		}

		/// <summary>
		/// Gets the name of the icon.
		/// </summary>
		public override string ToString() {
			return iconName.ToString(CultureInfo.InvariantCulture);
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private Texture RenderIcon(Color color) {
			var rect = icon.rect;

			if (iconMat == null) {
				iconMat = new Material(ShaderUtil.CreateShaderAsset(ICON_SHADER));
			}

			iconMat.SetColor("_Color", color);
			iconMat.SetVector("_TexelSize", new Vector2(1f / icon.texture.width, 1f / icon.texture.height));
			iconMat.SetVector("_Rect", new Vector4(rect.x / icon.texture.width, rect.y / icon.texture.height, rect.width / icon.texture.width, rect.height / icon.texture.height));

			var texture = new RenderTexture((int) rect.width, (int) rect.height, 0);
			Graphics.Blit(icon.texture, texture, iconMat);
			RenderTexture.active = null;

			return texture;
		}
	}
}