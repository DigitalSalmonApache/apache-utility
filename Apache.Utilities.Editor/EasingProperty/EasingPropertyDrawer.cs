﻿using UnityEditor;
using UnityEngine;

namespace Apache.Utilities.Editor {
	[CustomPropertyDrawer(typeof(EasingProperty))]
	public class EasingPropertyDrawer : PropertyDrawer {
		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
			EditorGUI.BeginProperty(position, label, property);

			position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

			SerializedProperty functionIndexProperty = property.FindPropertyRelative("FunctionIndex");
			if (functionIndexProperty != null) {
				functionIndexProperty.intValue = EditorGUI.Popup(position, functionIndexProperty.intValue, EasingProperty.EasingFunctionNames);
			}

			EditorGUI.EndProperty();
		}
	}
}