﻿using UnityEngine;

namespace Fayte {
	public enum AnimationSubStates {
		Enter,
		Exit
	}

	public interface 
		IAnimationStateHandler {
		void AnimationStateEvent(AnimatorStateInfo animatorStateInfo, AnimationSubStates animationSubState);
		void AnimationStateMachineEvent(int stateMachineHash, AnimationSubStates animationSubState);
	}
}