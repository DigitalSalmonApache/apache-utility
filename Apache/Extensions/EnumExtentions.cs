﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace Apache.Utilities {
	//-----------------------------------------------------------------------------------------
	// Attirbute Declaration:
	//-----------------------------------------------------------------------------------------

	public class FlagDrawerAttribute : PropertyAttribute {}

	public static class EnumExtensions {
		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		public static bool IsFlagSet<T>(this T value, T flag) where T : struct {
			CheckIsEnum<T>(true);
			long lValue = Convert.ToInt64(value);
			long lFlag = Convert.ToInt64(flag);
			return (lValue & lFlag) != 0;
		}

		public static bool AnyFlagMatches<T>(this T value, T flag) where T : struct {
			CheckIsEnum<T>(true);
			IEnumerable flags = flag.GetFlags();
			foreach (T flagValue in flags) {
				if (value.IsFlagSet(flagValue)) return true;
			}
			return false;
		}

		public static bool IsFlagSet<T>(this T value, int flag) where T : struct {
			CheckIsEnum<T>(true);
			long lValue = Convert.ToInt64(value);
			long lFlag = flag;
			return (lValue & lFlag) != 0;
		}

		public static bool IsFlagSet<T>(this int value, int flag) where T : struct {
			CheckIsEnum<T>(true);
			return (value & flag) != 0;
		}

		public static int IsFlagSetInt<T>(this T value, T flag) where T : struct {
			CheckIsEnum<T>(true);
			long lValue = Convert.ToInt64(value);
			long lFlag = Convert.ToInt64(flag);
			return ((lValue & lFlag) != 0) ? 1 : 0;
		}

		public static IEnumerable<T> GetFlags<T>(this T value) where T : struct {
			CheckIsEnum<T>(true);
			foreach (T flag in Enum.GetValues(typeof(T)).Cast<T>()) {
				if (value.IsFlagSet(flag)) yield return flag;
			}
		}

		public static T SetFlags<T>(this T value, T flags, bool on) where T : struct {
			CheckIsEnum<T>(true);
			long lValue = Convert.ToInt64(value);
			long lFlag = Convert.ToInt64(flags);
			if (on) {
				lValue |= lFlag;
			}
			else {
				lValue &= (~lFlag);
			}
			return (T) Enum.ToObject(typeof(T), lValue);
		}

		public static T SetFlags<T>(this T value, T flags) where T : struct {
			return value.SetFlags(flags, true);
		}

		public static T ClearFlags<T>(this T value, T flags) where T : struct {
			return value.SetFlags(flags, false);
		}

		public static T CombineFlags<T>(this IEnumerable<T> flags) where T : struct {
			CheckIsEnum<T>(true);
			long lValue = 0;
			foreach (T flag in flags) {
				long lFlag = Convert.ToInt64(flag);
				lValue |= lFlag;
			}
			return (T) Enum.ToObject(typeof(T), lValue);
		}

		public static string GetDescription<T>(this T value) where T : struct {
			CheckIsEnum<T>(false);
			string name = Enum.GetName(typeof(T), value);
			if (name != null) {
				FieldInfo field = typeof(T).GetField(name);
				if (field != null) {
					DescriptionAttribute attr = Attribute.GetCustomAttribute(field, typeof(DescriptionAttribute)) as DescriptionAttribute;
					if (attr != null) {
						return attr.Description;
					}
				}
			}
			return null;
		}

		public static IEnumerable<Enum> GetFlags(this Enum value) {
			return GetFlags(value, Enum.GetValues(value.GetType()).Cast<Enum>().ToArray());
		}

		public static IEnumerable<Enum> GetIndividualFlags(this Enum value) {
			return GetFlags(value, GetFlagValues(value.GetType()).ToArray());
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private static void CheckIsEnum<T>(bool withFlags) {
			if (!typeof(T).IsEnum) throw new ArgumentException(string.Format("Type '{0}' is not an enum", typeof(T).FullName));
			if (withFlags && !Attribute.IsDefined(typeof(T), typeof(FlagsAttribute))) throw new ArgumentException(string.Format("Type '{0}' doesn't have the 'Flags' attribute", typeof(T).FullName));
		}

		private static IEnumerable<Enum> GetFlags(Enum value, Enum[] values) {
			ulong bits = Convert.ToUInt64(value);
			List<Enum> results = new List<Enum>();
			for (int i = values.Length - 1; i >= 0; i--) {
				ulong mask = Convert.ToUInt64(values[i]);
				if (i == 0 && mask == 0L) break;
				if ((bits & mask) == mask) {
					results.Add(values[i]);
					bits -= mask;
				}
			}
			if (bits != 0L) return Enumerable.Empty<Enum>();
			if (Convert.ToUInt64(value) != 0L) return results.Reverse<Enum>();
			if (bits == Convert.ToUInt64(value) && values.Length > 0 && Convert.ToUInt64(values[0]) == 0L) return values.Take(1);
			return Enumerable.Empty<Enum>();
		}

		private static IEnumerable<Enum> GetFlagValues(Type enumType) {
			ulong flag = 0x1;
			foreach (var value in Enum.GetValues(enumType).Cast<Enum>()) {
				ulong bits = Convert.ToUInt64(value);
				if (bits == 0L)
					//yield return value;
					continue; // skip the zero value
				while (flag < bits) flag <<= 1;
				if (flag == bits) yield return value;
			}
		}
	}
}