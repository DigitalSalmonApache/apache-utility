﻿using Fayte;
using UnityEngine;

public abstract class AnimationHandler {
	//-----------------------------------------------------------------------------------------
	// Delegates:
	//-----------------------------------------------------------------------------------------

	public delegate void AnimationEventHandler();

	//-----------------------------------------------------------------------------------------
	// Events:
	//-----------------------------------------------------------------------------------------

	public event AnimationEventHandler OnAnimationComplete;

	//-----------------------------------------------------------------------------------------
	// Public Methods:
	//-----------------------------------------------------------------------------------------

	public abstract void PlayAnimation();

	protected void AnimationComplete() {
		if (OnAnimationComplete != null) OnAnimationComplete();
	}
}

public class AnimatorSequenceAnimationHandler : AnimationHandler, IAnimationStateHandler {
	//-----------------------------------------------------------------------------------------
	// Protected Fields:
	//-----------------------------------------------------------------------------------------
	protected string triggerParameter;

	protected string completeState;
	protected AnimationSubStates completeSubState;

	protected SubscribeAnimationMessenger[] messengers;
	protected Animator animator;

	//-----------------------------------------------------------------------------------------
	// Public Methods:
	//-----------------------------------------------------------------------------------------

	public AnimatorSequenceAnimationHandler(Animator handledAnimator, string triggerParam, string compState, AnimationSubStates subState) {
		animator = handledAnimator;
		triggerParameter = triggerParam;
		completeState = compState;
		completeSubState = subState;

		messengers = animator.GetBehaviours<SubscribeAnimationMessenger>();
		foreach (SubscribeAnimationMessenger messanger in messengers) {
			messanger.RegisterStateHandler(this);
		}
	}

	public void AnimationStateEvent(AnimatorStateInfo animatorStateInfo, AnimationSubStates animationSubState) {
		if (animatorStateInfo.IsName(completeState) && animationSubState == completeSubState) {
			AnimationComplete();
		}
	}

	public override void PlayAnimation() { animator.SetTrigger(triggerParameter); }
	public void AnimationStateMachineEvent(int stateMachineHash, AnimationSubStates animationSubState) { }
}