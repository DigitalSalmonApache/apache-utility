﻿using UnityEngine;

namespace Apache.Utilities {
	public static class VectorExtensions {
		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		/// <summary>
		/// Returns 'v' where 'v'.x|y are within range 'clampLow'.x|y -> 'clampHigh'.x|y.
		/// </summary>
		public static Vector2 Clamp(this Vector2 v, Vector2 clampLow, Vector2 clampHigh) {
			v.x = Mathf.Clamp(v.x, clampLow.x, clampHigh.x);
			v.y = Mathf.Clamp(v.y, clampLow.y, clampHigh.y);
			return v;
		}

		/// <summary>
		/// Returns whether 'v'.x|y are both within 0 -> 1 range.
		/// </summary>
		public static bool IsWithin01Range(this Vector2 v) {
			return (v.x >= 0 && v.x <= 1 && v.y >= 0 && v.y <= 1);
		}

		/// <summary>
		/// Returns whether 'v'.x|y are both within -1 -> 1 range.
		/// </summary>
		public static bool IsWithinNormalisedRange(this Vector2 v) {
			return (v.x >= -1 && v.x <= 1 && v.y >= -1 && v.y <= 1);
		}

		/// <summary>
		/// Round 'a'.x|y|z.
		/// </summary>
		public static Vector3 Snap(this Vector3 a) {
			return a.Snap(1f);
		}

		/// <summary>
		/// Round 'a'.x|y|z to the nearest 'snapValue'.
		/// </summary>
		public static Vector3 Snap(this Vector3 a, float snapValue) {
			return a.Snap(new Vector3(snapValue, snapValue, snapValue));
		}

		/// <summary>
		/// Round 'a'.x|y|z to the nearest 'snapValue'.x|y|z respectively.
		/// </summary>
		public static Vector3 Snap(this Vector3 a, Vector3 snapValue) {
			a.x = a.x.Snap(snapValue.x);
			a.y = a.y.Snap(snapValue.y);
			a.z = a.z.Snap(snapValue.z);
			return a;
		}

		/// <summary>
		/// Round 'a'.x|y.
		/// </summary>
		public static Vector2 Snap(this Vector2 a) {
			return a.Snap(1f);
		}

		/// <summary>
		/// Round 'a'.x|y to the nearest 'snapValue'.
		/// </summary>
		public static Vector2 Snap(this Vector2 a, float snapValue) {
			return a.Snap(new Vector2(snapValue, snapValue));
		}

		/// <summary>
		/// Round 'a'.x|y to the nearest 'snapValue'.x|y respectively.
		/// </summary>
		public static Vector2 Snap(this Vector2 a, Vector2 snapValue) {
			a.x = a.x.Snap(snapValue.x);
			a.y = a.y.Snap(snapValue.y);
			return a;
		}

		/// <summary>
		/// Round 'a'.
		/// </summary>
		public static float Snap(this float a) {
			return Mathf.Round(a);
		}

		/// <summary>
		/// Round 'a' to the nearest 'snapValue'.
		/// </summary>
		public static float Snap(this float a, float snapValue) {
			if (snapValue == 0) return a;

			return Mathf.Round(a * snapValue) / snapValue;
		}
	}
}