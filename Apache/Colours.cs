﻿using UnityEngine;

public static class Colours {
	//-----------------------------------------------------------------------------------------
	// Constants:
	//-----------------------------------------------------------------------------------------

	// Apache:

	public static readonly Color APACHE_BLUE = new Color(0.235f, 0.604f, 0.862f);
	public static readonly Color APACHE_RICH_BLUE = new Color(0.055f, 0.424f, 0.702f);
	public static readonly Color APACHE_LIGHT_BLUE = new Color(0.573f, 0.745f, 0.922f);

	public static readonly Color APACHE_BLUE_BLACK = new Color(0.168f, 0.180f, 0.188f);
	public static readonly Color APACHE_OFF_BLACK = new Color(0.129f, 0.129f, 0.129f);

	// Greyscale:

	public static readonly Color BLACK = Color.black;

	public static readonly Color DARKEST_GREY = new Color(0.05f, 0.05f, 0.05f);
	public static readonly Color DARKER_GREY = new Color(0.08f, 0.08f, 0.08f);
	public static readonly Color DARK_GREY = new Color(0.15f, 0.15f, 0.15f);
	public static readonly Color DARKISH_GREY = new Color(0.2f, 0.2f, 0.2f);
	public static readonly Color LIGHTER_GREY = new Color(0.92f, 0.92f, 0.92f);
	public static readonly Color LIGHTEST_GREY = new Color(0.95f, 0.95f, 0.95f);
	public static readonly Color LIGHT_GREY = new Color(0.85f, 0.85f, 0.85f);

	public static readonly Color LIGHTISH_GREY = new Color(0.8f, 0.8f, 0.8f);
	public static readonly Color MID_DARKER_GREY = new Color(0.3f, 0.3f, 0.3f);

	public static readonly Color MID_DARKEST_GREY = new Color(0.25f, 0.25f, 0.25f);
	public static readonly Color MID_DARK_GREY = new Color(0.35f, 0.35f, 0.35f);
	public static readonly Color MID_DARKISH_GREY = new Color(0.42f, 0.42f, 0.42f);

	public static readonly Color MID_GREY = new Color(0.5f, 0.5f, 0.5f);
	public static readonly Color MID_LIGHTER_GREY = new Color(0.7f, 0.7f, 0.7f);
	public static readonly Color MID_LIGHTEST_GREY = new Color(0.75f, 0.75f, 0.75f);
	public static readonly Color MID_LIGHT_GREY = new Color(0.65f, 0.65f, 0.65f);

	public static readonly Color MID_LIGHTISH_GREY = new Color(0.58f, 0.58f, 0.58f);

	public static readonly Color OFF_BLACK = new Color(0.02f, 0.02f, 0.02f);
	public static readonly Color OFF_WHITE = new Color(0.98f, 0.98f, 0.98f);

	public static readonly Color WHITE = Color.white;
}