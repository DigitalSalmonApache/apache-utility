﻿using System;
using System.Globalization;
using UnityEngine;

namespace Apache.Utilities {
	public static class ColorExtensions {
		//-----------------------------------------------------------------------------------------
		// Constants:
		//-----------------------------------------------------------------------------------------

		private static readonly char[] TRIM_RGB_START = new char[] {'R', 'r', 'G', 'g', 'B', 'b', 'A', 'a', '('};

		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------
		
		/// <summary>
		/// Lerps between multiple colors.
		/// </summary>
		/// <param name="colors">The colors.</param>
		/// <param name="t">The t.</param>
		/// <returns></returns>
		public static Color Lerp(this Color[] colors, float t) {
			t = Mathf.Clamp(t, 0, 1) * (colors.Length - 1);
			int a = (int) t;
			int b = Mathf.Min((int) t + 1, colors.Length - 1);
			return Color.Lerp(colors[a], colors[b], t - (int) t);
		}

		/// <summary>
		/// Moves the towards implementation for Color.
		/// </summary>
		/// <param name="from">From color.</param>
		/// <param name="to">To color.</param>
		/// <param name="maxDelta">The maximum delta.</param>
		public static Color MoveTowards(this Color from, Color to, float maxDelta) {
			Color result = new Color();
			result.r = Mathf.MoveTowards(from.r, to.r, maxDelta);
			result.g = Mathf.MoveTowards(from.g, to.g, maxDelta);
			result.b = Mathf.MoveTowards(from.b, to.b, maxDelta);
			result.a = Mathf.MoveTowards(from.a, to.a, maxDelta);

			from.r = result.r;
			from.g = result.g;
			from.b = result.b;
			from.a = result.a;

			return result;
		}

		/// <summary>
		/// Tries to parse a string to a Color. The following formats are supported:
		/// "new Color(0.4, 0, 0, 1)", "#FFEEBBFF", "#FFEECC", "FFEEBBFF", "FFEECC"
		/// </summary>
		/// <param name="colorStr">The color string.</param>
		/// <param name="color">The color.</param>
		/// <returns>Returns true if the parse was a success.</returns>
		public static bool TryParseString(string colorStr, out Color color) {
			color = default(Color);

			if (colorStr == null || colorStr.Length < 2 || colorStr.Length > 100) {
				return false;
			}

			if (colorStr.StartsWith("new Color", StringComparison.InvariantCulture)) {
				colorStr = colorStr.Substring("new Color".Length, colorStr.Length - "new Color".Length).Replace("f", "");
			}

			bool couldBeHex = colorStr[0] == '#' || char.IsLetter(colorStr[0]) || char.IsNumber(colorStr[0]);
			bool couldBeRGB = colorStr[0] == 'R' || colorStr[0] == '(' || char.IsNumber(colorStr[0]);

			if (couldBeHex == false && couldBeRGB == false) {
				return false;
			}
			bool didConvert = false;
			if (couldBeRGB || couldBeHex && (didConvert = ColorUtility.TryParseHtmlString(colorStr, out color)) == false && couldBeRGB) {
				colorStr = colorStr.TrimStart(TRIM_RGB_START).TrimEnd(')');
				string[] components = colorStr.Split(',');
				if (components.Length < 2 || components.Length > 4) {
					return false;
				}

				Color result = new Color(0, 0, 0, 1);
				for (int i = 0; i < components.Length; i++) {
					float component;
					if (float.TryParse(components[i], out component) == false) {
						return false;
					}

					if (i == 0) result.r = component;
					if (i == 1) result.g = component;
					if (i == 2) result.b = component;
					if (i == 3) result.a = component;
				}
				color = result;
				return true;
			}
			else if (didConvert) {
				return true;
			}
			return false;
		}

		/// <summary>
		/// Converts a color to a string formatted to c#
		/// </summary>
		/// <param name="color">The color.</param>
		/// <returns>new Color(r, g, b, a)</returns>
		public static string ToCSharpColor(this Color color) {
			return "new Color(" + TrimFloat(color.r) + "f, " + TrimFloat(color.g) + "f, " + TrimFloat(color.b) + "f, " + TrimFloat(color.a) + "f)";
		}

		/// <summary>
		/// Pows the color with the specified factor.
		/// </summary>
		/// <param name="color">The color.</param>
		/// <param name="factor">The factor.</param>
		public static Color Pow(this Color color, float factor) {
			color.r = Mathf.Pow(color.r, factor);
			color.g = Mathf.Pow(color.g, factor);
			color.b = Mathf.Pow(color.b, factor);
			color.a = Mathf.Pow(color.a, factor);
			return color;
		}

		/// <summary>
		/// Normalizes the RGB values of the color ignoring the alpha value.
		/// </summary>
		/// <param name="color">The color.</param>
		public static Color NormalizeRGB(this Color color) {
			Vector3 c = new Vector3(color.r, color.g, color.b).normalized;
			color.r = c.x;
			color.g = c.y;
			color.b = c.z;
			return color;
		}

		/// <summary>
		/// Convert an rgb color to hsv.
		/// </summary>
		public static Vector3 ToHsv(this Color rgb) {
			float r = rgb.r / 255;
			float g = rgb.r / 255;
			float b = rgb.r / 255;
			float v;
			float m;
			float vm;
			float r2, g2, b2;

			float h = 0; // default to black
			float s = 0;
			float l = 0;
			v = Mathf.Max(r, g);
			v = Mathf.Max(v, b);
			m = Mathf.Min(r, g);
			m = Mathf.Min(m, b);
			l = (m + v) / 2;
			if (l <= 0) {
				return new Vector3(h, s, l);
			}
			vm = v - m;
			s = vm;
			if (s > 0) {
				s /= (l <= 0.5f) ? (v + m) : (2 - v - m);
			}
			else {
				return new Vector3(h, s, l);
			}
			r2 = (v - r) / vm;
			g2 = (v - g) / vm;
			b2 = (v - b) / vm;
			if (r == v) {
				h = (g == m ? 5 + b2 : 1 - g2);
			}
			else if (g == v) {
				h = (b == m ? 1 + r2 : 3 - b2);
			}
			else {
				h = (r == m ? 3 + g2 : 5 - r2);
			}
			h /= 6;

			return new Vector3(h, s, l);
		}

		/// <summary>
		/// Convert an rgb color to hsb.
		/// </summary>
		public static Vector3 ToHsb(this Color color) {
			Vector3 ret = Vector3.zero;

			float r = color.r;
			float g = color.g;
			float b = color.b;

			float max = Mathf.Max(r, Mathf.Max(g, b));

			if (max <= 0) {
				return ret;
			}

			float min = Mathf.Min(r, Mathf.Min(g, b));
			float dif = max - min;

			if (max > min) {
				if (g == max) {
					ret.x = (b - r) / dif * 60f + 120f;
				}
				else if (b == max) {
					ret.x = (r - g) / dif * 60f + 240f;
				}
				else if (b > g) {
					ret.x = (g - b) / dif * 60f + 360f;
				}
				else {
					ret.x = (g - b) / dif * 60f;
				}
				if (ret.x < 0) {
					ret.x = ret.x + 360f;
				}
			}
			else {
				ret.x = 0;
			}

			ret.x *= 1f / 360f;
			ret.y = (dif / max) * 1f;
			ret.z = max;

			return ret;
		}

		/// <summary>
		/// Return 'color' where 'color'.a = alpha.
		/// </summary>
		public static Color WithAlpha(this Color color, float alpha) {
			color.a = alpha;
			return color;
		}

		/// <summary>
		/// Lerp between 'a' and 'b' by 't' without clamping.
		/// </summary>
		public static Color ULerp(Color a, Color b, float t) {
			return new Color(a.r + (b.r - a.r) * t, a.g + (b.g - a.g) * t, a.b + (b.b - a.b) * t, a.a + (b.a - a.a) * t);
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		private static string TrimFloat(float value) {
			string str = value.ToString("F3", CultureInfo.InvariantCulture).TrimEnd('0');
			char lastChar = str[str.Length - 1];
			if (lastChar == '.' || lastChar == ',') {
				str = str.Substring(0, str.Length - 1);
			}
			return str;
		}
	}
}