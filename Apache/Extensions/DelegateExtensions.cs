using System;
using System.Collections.Generic;

namespace Apache.Utilities {
	public static class DelegateExtensions {
		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		/// <summary>
		/// Safely invoke an action if it is valid.
		/// </summary>
		public static void SafeInvoke(this Action self) {
			if (self == null) return;
			self();
		}

		/// <summary>
		/// Safely invoke an action if it is valid.
		/// </summary>
		public static void SafeInvoke<T>(this Action<T> self, T value) {
			if (self == null) return;
			self(value);
		}

		/// <summary>
		/// Memoizes the specified func - returns the memoized version
		/// </summary>
		public static Func<TResult> Memoize<TResult>(this Func<TResult> getValue) {
			TResult value = default(TResult);
			bool hasValue = false;
			return () => {
				if (!hasValue) {
					hasValue = true;
					value = getValue();
				}

				return value;
			};
		}

		/// <summary>
		/// Memoizes the specified func - returns the memoized version
		/// </summary>
		public static Func<T, TResult> Memoize<T, TResult>(this Func<T, TResult> func) {
			var dic = new Dictionary<T, TResult>();
			return n => {
				TResult result;
				if (!dic.TryGetValue(n, out result)) {
					result = func(n);
					dic.Add(n, result);
				}

				return result;
			};
		}
	}
}