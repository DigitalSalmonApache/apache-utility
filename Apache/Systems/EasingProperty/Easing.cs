﻿using System;

namespace Apache.Utilities {

	// All methods are defined twice, rather than using optional parameters.
	// This is to help with dynamic Func<float,float> generation,
	// as optional parameters complicate the process.
	public class Easing {
		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		public static EasingFunctionDeclaration[] CustomEasingDeclarations() { return null; }

		public static float Linear(float t) { return Linear(t, 0, 1); }

		public static float Linear(float t, float b, float c, float d = 1) { return c * t / d + b; }

		public static float ExpoEaseOut(float t) { return ExpoEaseOut(t, 0, 1); }

		public static float ExpoEaseOut(float t, float b, float c, float d = 1) { return (float) ((t == d) ? b + c : c * (-Math.Pow(2, -10 * t / d) + 1) + b); }

		public static float ExpoEaseIn(float t) { return ExpoEaseIn(t, 0, 1); }

		public static float ExpoEaseIn(float t, float b, float c, float d = 1) { return (float) ((t == 0) ? b : c * Math.Pow(2, 10 * (t / d - 1)) + b); }

		public static float ExpoEaseInOut(float t) { return ExpoEaseInOut(t, 0, 1); }

		public static float ExpoEaseInOut(float t, float b, float c, float d = 1) {
			if (t == 0) return b;

			if (t == d) return b + c;

			if ((t /= d / 2) < 1) return (float) (c / 2 * Math.Pow(2, 10 * (t - 1)) + b);

			return (float) (c / 2 * (-Math.Pow(2, -10 * --t) + 2) + b);
		}

		public static float ExpoEaseOutIn(float t) { return ExpoEaseOutIn(t, 0, 1); }

		public static float ExpoEaseOutIn(float t, float b, float c, float d = 1) {
			if (t < d / 2) return ExpoEaseOut(t * 2, b, c / 2, d);

			return ExpoEaseIn((t * 2) - d, b + c / 2, c / 2, d);
		}

		public static float CircEaseOut(float t) { return CircEaseOut(t, 0, 1); }

		public static float CircEaseOut(float t, float b, float c, float d = 1) { return (float) (c * Math.Sqrt(1 - (t = t / d - 1) * t) + b); }

		public static float CircEaseIn(float t) { return CircEaseIn(t, 0, 1); }

		public static float CircEaseIn(float t, float b, float c, float d = 1) { return (float) (-c * (Math.Sqrt(1 - (t /= d) * t) - 1) + b); }

		public static float CircEaseInOut(float t) { return CircEaseInOut(t, 0, 1); }

		public static float CircEaseInOut(float t, float b, float c, float d = 1) {
			if ((t /= d / 2) < 1) return (float) (-c / 2 * (Math.Sqrt(1 - t * t) - 1) + b);

			return (float) (c / 2 * (Math.Sqrt(1 - (t -= 2) * t) + 1) + b);
		}

		public static float CircEaseOutIn(float t) { return CircEaseOutIn(t, 0, 1); }

		public static float CircEaseOutIn(float t, float b, float c, float d = 1) {
			if (t < d / 2) return CircEaseOut(t * 2, b, c / 2, d);

			return CircEaseIn((t * 2) - d, b + c / 2, c / 2, d);
		}

		public static float QuadEaseOut(float t) { return QuadEaseOut(t, 0, 1); }

		public static float QuadEaseOut(float t, float b, float c, float d = 1) { return -c * (t /= d) * (t - 2) + b; }

		public static float QuadEaseIn(float t) { return QuadEaseIn(t, 0, 1); }

		public static float QuadEaseIn(float t, float b, float c, float d = 1) { return c * (t /= d) * t + b; }

		public static float QuadEaseInOut(float t) { return QuadEaseInOut(t, 0, 1); }

		public static float QuadEaseInOut(float t, float b, float c, float d = 1) {
			if ((t /= d / 2) < 1) return c / 2 * t * t + b;
			return -c / 2 * ((--t) * (t - 2) - 1) + b;
		}

		public static float QuadEaseOutIn(float t) { return QuadEaseOutIn(t, 0, 1); }

		public static float QuadEaseOutIn(float t, float b, float c, float d = 1) {
			if (t < d / 2) return QuadEaseOut(t * 2, b, c / 2, d);
			return QuadEaseIn((t * 2) - d, b + c / 2, c / 2, d);
		}

		public static float SineEaseOut(float t) { return SineEaseOut(t, 0, 1); }

		public static float SineEaseOut(float t, float b, float c, float d = 1) { return (float) (c * Math.Sin(t / d * (Math.PI / 2)) + b); }

		public static float SineEaseIn(float t) { return SineEaseIn(t, 0, 1); }

		public static float SineEaseIn(float t, float b, float c, float d = 1) { return (float) (-c * Math.Cos(t / d * (Math.PI / 2)) + c + b); }

		public static float SineEaseInOut(float t) { return SineEaseInOut(t, 0, 1); }

		public static float SineEaseInOut(float t, float b, float c, float d = 1) {
			if ((t /= d / 2) < 1) return (float) (c / 2 * (Math.Sin(Math.PI * t / 2)) + b);

			return (float) (-c / 2 * (Math.Cos(Math.PI * --t / 2) - 2) + b);
		}

		public static float SineEaseOutIn(float t) { return SineEaseOutIn(t, 0, 1); }

		public static float SineEaseOutIn(float t, float b, float c, float d = 1) {
			if (t < d / 2) return SineEaseOut(t * 2, b, c / 2, d);

			return SineEaseIn((t * 2) - d, b + c / 2, c / 2, d);
		}

		public static float CubicEaseOut(float t) { return CubicEaseOut(t, 0, 1); }

		public static float CubicEaseOut(float t, float b, float c, float d = 1) { return c * ((t = t / d - 1) * t * t + 1) + b; }

		public static float CubicEaseIn(float t) { return CubicEaseIn(t, 0, 1); }

		public static float CubicEaseIn(float t, float b, float c, float d = 1) { return c * (t /= d) * t * t + b; }

		public static float CubicEaseInOut(float t) { return CubicEaseInOut(t, 0, 1); }

		public static float CubicEaseInOut(float t, float b, float c, float d = 1) {
			if ((t /= d / 2) < 1) return c / 2 * t * t * t + b;

			return c / 2 * ((t -= 2) * t * t + 2) + b;
		}

		public static float CubicEaseOutIn(float t) { return CubicEaseOutIn(t, 0, 1); }

		public static float CubicEaseOutIn(float t, float b, float c, float d = 1) {
			if (t < d / 2) return CubicEaseOut(t * 2, b, c / 2, d);

			return CubicEaseIn((t * 2) - d, b + c / 2, c / 2, d);
		}

		public static float QuartEaseOut(float t) { return QuartEaseOut(t, 0, 1); }

		public static float QuartEaseOut(float t, float b, float c, float d = 1) { return -c * ((t = t / d - 1) * t * t * t - 1) + b; }

		public static float QuartEaseIn(float t) { return QuartEaseIn(t, 0, 1); }

		public static float QuartEaseIn(float t, float b, float c, float d = 1) { return c * (t /= d) * t * t * t + b; }

		public static float QuartEaseInOut(float t) { return QuartEaseInOut(t, 0, 1); }

		public static float QuartEaseInOut(float t, float b, float c, float d = 1) {
			if ((t /= d / 2) < 1) return c / 2 * t * t * t * t + b;

			return -c / 2 * ((t -= 2) * t * t * t - 2) + b;
		}

		public static float QuartEaseOutIn(float t) { return QuartEaseOutIn(t, 0, 1); }

		public static float QuartEaseOutIn(float t, float b, float c, float d = 1) {
			if (t < d / 2) return QuartEaseOut(t * 2, b, c / 2, d);

			return QuartEaseIn((t * 2) - d, b + c / 2, c / 2, d);
		}

		public static float QuintEaseOut(float t) { return QuintEaseOut(t, 0, 1); }

		public static float QuintEaseOut(float t, float b, float c, float d = 1) { return c * ((t = t / d - 1) * t * t * t * t + 1) + b; }

		public static float QuintEaseIn(float t) { return QuintEaseIn(t, 0, 1); }

		public static float QuintEaseIn(float t, float b, float c, float d = 1) { return c * (t /= d) * t * t * t * t + b; }

		public static float QuintEaseInOut(float t) { return QuintEaseInOut(t, 0, 1); }

		public static float QuintEaseInOut(float t, float b, float c, float d = 1) {
			if ((t /= d / 2) < 1) return c / 2 * t * t * t * t * t + b;
			return c / 2 * ((t -= 2) * t * t * t * t + 2) + b;
		}

		public static float QuintEaseOutIn(float t) { return QuintEaseOutIn(t, 0, 1); }

		public static float QuintEaseOutIn(float t, float b, float c, float d = 1) {
			if (t < d / 2) return QuintEaseOut(t * 2, b, c / 2, d);
			return QuintEaseIn((t * 2) - d, b + c / 2, c / 2, d);
		}

		public static float ElasticEaseIn(float t) { return ElasticEaseIn(t, 0, 1); }

		public static float ElasticEaseIn(float t, float b, float c, float d = 1) {
			if ((t /= d) == 1) return b + c;

			float p = d * .3f;
			float s = p / 4;

			return (float) (-(c * Math.Pow(2, 10 * (t -= 1)) * Math.Sin((t * d - s) * (2 * Math.PI) / p)) + b);
		}

		public static float ElasticEaseOutIn(float t) { return ElasticEaseOutIn(t, 0, 1); }

		public static float ElasticEaseOutIn(float t, float b, float c, float d = 1) {
			if (t < d / 2) return ElasticEaseOut(t * 2, b, c / 2, d);
			return ElasticEaseIn((t * 2) - d, b + c / 2, c / 2, d);
		}

		public static float BounceEaseOut(float t) { return BounceEaseOut(t, 0, 1); }

		public static float BounceEaseOut(float t, float b, float c, float d = 1) {
			if ((t /= d) < (1 / 2.75)) return c * (7.5625f * t * t) + b;
			if (t < (2 / 2.75f)) return c * (7.5625f * (t -= (1.5f / 2.75f)) * t + .75f) + b;
			if (t < (2.5 / 2.75)) return c * (7.5625f * (t -= (2.25f / 2.75f)) * t + .9375f) + b;
			return c * (7.5625f * (t -= (2.625f / 2.75f)) * t + .984375f) + b;
		}

		public static float BounceEaseIn(float t) { return BounceEaseIn(t, 0, 1); }

		public static float BounceEaseIn(float t, float b, float c, float d = 1) { return c - BounceEaseOut(d - t, 0, c, d) + b; }

		public static float BounceEaseInOut(float t) { return BounceEaseInOut(t, 0, 1); }

		public static float BounceEaseInOut(float t, float b, float c, float d = 1) {
			if (t < d / 2) return BounceEaseIn(t * 2, 0, c, d) * .5f + b;
			return BounceEaseOut(t * 2 - d, 0, c, d) * .5f + c * .5f + b;
		}

		public static float BounceEaseOutIn(float t) { return BounceEaseOutIn(t, 0, 1); }

		public static float BounceEaseOutIn(float t, float b, float c, float d = 1) {
			if (t < d / 2) return BounceEaseOut(t * 2, b, c / 2, d);
			return BounceEaseIn((t * 2) - d, b + c / 2, c / 2, d);
		}

		public static float BackEaseOut(float t) { return BackEaseOut(t, 0, 1); }

		public static float BackEaseOut(float t, float b, float c, float d = 1) { return c * ((t = t / d - 1) * t * ((1.70158f + 1) * t + 1.70158f) + 1) + b; }

		public static float BackEaseIn(float t) { return BackEaseIn(t, 0, 1); }

		public static float BackEaseIn(float t, float b, float c, float d = 1) { return c * (t /= d) * t * ((1.70158f + 1) * t - 1.70158f) + b; }

		public static float BackEaseInOut(float t) { return BackEaseInOut(t, 0, 1); }

		public static float BackEaseInOut(float t, float b, float c, float d = 1) {
			float s = 1.70158f;
			if ((t /= d / 2) < 1) return c / 2 * (t * t * (((s *= (1.525f)) + 1) * t - s)) + b;
			return c / 2 * ((t -= 2) * t * (((s *= (1.525f)) + 1) * t + s) + 2) + b;
		}

		public static float BackEaseOutIn(float t) { return BackEaseOutIn(t, 0, 1); }

		public static float BackEaseOutIn(float t, float b, float c, float d = 1) {
			if (t < d / 2) return BackEaseOut(t * 2, b, c / 2, d);
			return BackEaseIn((t * 2) - d, b + c / 2, c / 2, d);
		}

		public static float ElasticEaseOut(float t) { return ElasticEaseOut(t, 0, 1); }

		public static float ElasticEaseOut(float t, float b, float c, float d = 1) {
			if ((t /= d) == 1) return b + c;

			float p = d * .3f;
			float s = p / 4;

			return (float) ((c * Math.Pow(2, -10 * t) * Math.Sin((t * d - s) * (2 * Math.PI) / p) + c + b));
		}

		public static float ElasticEaseOutDampened(float t) { return ElasticEaseOutDampened(t, 0, 1); }

		public static float ElasticEaseOutDampened(float t, float b, float c, float d = 1) {
			if ((t /= d) == 1) return b + c;

			float p = d * .3f;
			float s = p / 4;

			return (float) ((c * Math.Pow(2, -15 * t) * Math.Sin((t * d - s) * (2 * Math.PI) / p) + c + b));
		}

		public static float ElasticEaseInOut(float t) { return ElasticEaseInOut(t, 0, 1); }

		public static float ElasticEaseInOut(float t, float b, float c, float d = 1) {
			if ((t /= d / 2) == 2) return b + c;

			float p = d * (.3f * 1.5f);
			float s = p / 4;

			if (t < 1) return (float) (-0.5f * (c * Math.Pow(2, 10 * (t -= 1)) * Math.Sin((t * d - s) * (2 * Math.PI) / p)) + b);
			return (float) (c * Math.Pow(2, -10 * (t -= 1)) * Math.Sin((t * d - s) * (2 * Math.PI) / p) * .5f + c + b);
		}

		public static float ElasticEaseInOutDampened(float t) { return ElasticEaseInOutDampened(t, 0, 1); }

		public static float ElasticEaseInOutDampened(float t, float b, float c, float d = 1) {
			if ((t /= d / 2) == 2) return b + c;

			float p = d * (.3f * 1.5f);
			float s = p / 4;

			if (t < 1) return (float) (-0.5f * (c * Math.Pow(2, 15 * (t -= 1)) * Math.Sin((t * d - s) * (2 * Math.PI) / p)) + b);
			return (float) (c * Math.Pow(2, -15 * (t -= 1)) * Math.Sin((t * d - s) * (2 * Math.PI) / p) * .5f + c + b);
		}
	}
}