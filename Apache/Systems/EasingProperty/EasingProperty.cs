﻿using System;
using System.Linq;
using System.Reflection;

namespace Apache.Utilities {
	//-----------------------------------------------------------------------------------------
	// Type Declaration:
	//-----------------------------------------------------------------------------------------

	public class EasingFunctionDeclaration {
		public Func<float, float> Func;
		public string Name;

		public EasingFunctionDeclaration(string name, Func<float, float> func) {
			Name = name;
			Func = func;
		}
	}

	[Serializable]
	public class EasingProperty {
		//-----------------------------------------------------------------------------------------
		// Public Fields:
		//-----------------------------------------------------------------------------------------

		public int FunctionIndex = 0;
		public string FunctionName;

		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private EasingFunctionDeclaration selectedFunction;

		private static EasingFunctionDeclaration[] easingFunctions;
		private static string[] easingFunctionNames;

		private static EasingFunctionDeclaration fallbackFuncDeclaration;

		public static string[] EasingFunctionNames { get { return easingFunctionNames ?? (easingFunctionNames = GetEasingFunctionNames()); } set { easingFunctionNames = value; } }
		public static EasingFunctionDeclaration[] EasingFunctions { get { return easingFunctions ?? (easingFunctions = GetEasingFunctions()); } set { easingFunctions = value; } }

		//-----------------------------------------------------------------------------------------
		// Public Properties:
		//-----------------------------------------------------------------------------------------

		public Func<float, float> Function { get { return (selectedFunction ?? (selectedFunction = GetFunc(FunctionName, FunctionIndex))).Func; } }
		private static EasingFunctionDeclaration FallbackFuncDeclaration { get { return fallbackFuncDeclaration ?? (fallbackFuncDeclaration = GetFallbackFuncDeclaration()); } }

		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		public float Evaluate(float t) {
			return Function(t);
		}

		//-----------------------------------------------------------------------------------------
		// Private Methods:
		//-----------------------------------------------------------------------------------------

		// ReSharper disable once UnusedMember.Local
		private float FallbackFuncMethod(float a) {
			return a;
		}

		private static EasingFunctionDeclaration GetFunc(string funcName, int funcIndex) {
			EasingFunctionDeclaration funcFromName = GetFuncFromName(funcName);
			if (funcFromName != FallbackFuncDeclaration) return funcFromName;
			EasingFunctionDeclaration funcFromIndex = GetFuncFromIndex(funcIndex);
			return funcFromIndex;
		}

		private static EasingFunctionDeclaration GetFuncFromName(string name) {
			return EasingFunctions.FirstOrDefault(f => f.Name == name) ?? FallbackFuncDeclaration;
		}

		private static EasingFunctionDeclaration GetFuncFromIndex(int index) {
			return EasingFunctions[index] == null ? FallbackFuncDeclaration : EasingFunctions[index];
		}

		private static string[] GetEasingFunctionNames() {
			return GetEasingFunctions().Select(f => f.Name).ToArray();
		}

		private static EasingFunctionDeclaration[] GetEasingFunctions() {
			MethodInfo[] functions = typeof(Easing).GetMethods().Where(m => m.GetParameters().Length == 1 && m.GetParameters().First().ParameterType == typeof(float)).ToArray();
			return functions.Select(f => new EasingFunctionDeclaration(f.Name, (Func<float, float>) Delegate.CreateDelegate(typeof(Func<float, float>), f))).ToArray();
		}

		private static EasingFunctionDeclaration GetFallbackFuncDeclaration() {
			return new EasingFunctionDeclaration("Fallback", (Func<float, float>) Delegate.CreateDelegate(typeof(Func<float, float>), typeof(EasingProperty).GetMethod("FallbackFuncMethod")));
		}
	}
}