using UnityEditor;
using UnityEngine;

namespace Apache.Utilities.Editor {
	public static class EditorIcons {
		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private static Texture2D unityInfoIcon;
		private static Texture2D unityWarningIcon;
		private static Texture2D unityErrorIcon;

		/*
		private static EditorIcon example;

		/// <summary>
		/// Gets an icon of an airplane.
		/// </summary>
		public static EditorIcon Example { get { return example ?? (example = new LazyEditorIcon("example")); } }
		*/

		//-----------------------------------------------------------------------------------------
		// Public Properties:
		//-----------------------------------------------------------------------------------------

		/// <summary>
		/// Gets an icon of a unity info icon.
		/// </summary>
		public static Texture2D UnityInfoIcon {
			get { return unityInfoIcon ?? (unityInfoIcon = (Texture2D) typeof(EditorGUIUtility).GetMethod("LoadIcon", Flags.ALL_MEMBERS).Invoke(null, new object[] {"console.infoicon"})); }
		}

		/// <summary>
		/// Gets an icon of a unity warning icon.
		/// </summary>
		public static Texture2D UnityWarningIcon {
			get {
				return unityWarningIcon ?? (unityWarningIcon = (Texture2D) typeof(EditorGUIUtility).GetMethod("LoadIcon", Flags.ALL_MEMBERS).Invoke(null, new object[] {"console.warnicon"}));
			}
		}

		/// <summary>
		/// Gets an icon of a unity error icon.
		/// </summary>
		public static Texture2D UnityErrorIcon {
			get {
				return unityErrorIcon ?? (unityErrorIcon = (Texture2D) typeof(EditorGUIUtility).GetMethod("LoadIcon", Flags.ALL_MEMBERS).Invoke(null, new object[] {"console.erroricon"}));
			}
		}
	}
}