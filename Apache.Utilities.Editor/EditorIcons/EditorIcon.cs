using UnityEngine;

namespace Apache.Utilities.Editor {
	public abstract class EditorIcon {
		//-----------------------------------------------------------------------------------------
		// Private Fields:
		//-----------------------------------------------------------------------------------------

		private GUIContent inactiveGUIContent;
		private GUIContent highlightedGUIContent;
		private GUIContent activeGUIContent;

		//-----------------------------------------------------------------------------------------
		// Public Properties:
		//-----------------------------------------------------------------------------------------

		/// <summary>
		/// Gets the icon's highlighted texture.
		/// </summary>
		public abstract Texture Highlighted { get; }

		/// <summary>
		/// Gets the icon's active texture.
		/// </summary>
		public abstract Texture Active { get; }

		/// <summary>
		/// Gets the icon's inactive texture.
		/// </summary>
		public abstract Texture Inactive { get; }

		/// <summary>
		/// Gets a GUIContent object with the active texture.
		/// </summary>
		public GUIContent ActiveGUIContent { get { return activeGUIContent ?? (activeGUIContent = new GUIContent(Active)); } }

		/// <summary>
		/// Gets a GUIContent object with the inactive texture.
		/// </summary>
		public GUIContent InactiveGUIContent { get { return inactiveGUIContent ?? (inactiveGUIContent = new GUIContent(Inactive)); } }

		/// <summary>
		/// Gets a GUIContent object with the highlighted texture.
		/// </summary>
		public GUIContent HighlightedGUIContent { get { return highlightedGUIContent ?? (highlightedGUIContent = new GUIContent(Highlighted)); } }
	}
}