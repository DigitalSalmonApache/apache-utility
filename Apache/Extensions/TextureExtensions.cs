﻿using UnityEngine;

namespace Apache.Utilities {
	public static class TextureExtensions {
		//-----------------------------------------------------------------------------------------
		// Public Methods:
		//-----------------------------------------------------------------------------------------

		/// <summary>
		/// Attempts to determine if a texture is readable by reading a single pixel.
		/// </summary>
		public static bool IsReadable(this Texture2D texture2D) {
			try {
				texture2D.GetPixel(0, 0);
				return true;
			}
			catch {
				return false;
			}
		}
	}
}