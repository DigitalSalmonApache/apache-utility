﻿using System.Collections.Generic;
using System.Linq;
using Fayte;
using UnityEngine;

public class BroadcastAnimationMessenger : StateMachineBehaviour {
	//-----------------------------------------------------------------------------------------
	// Inspector Variables:
	//-----------------------------------------------------------------------------------------
	[SerializeField] protected bool cache;

	//-----------------------------------------------------------------------------------------
	// Private Fields:
	//-----------------------------------------------------------------------------------------

	private bool cached;
	private List<IAnimationStateHandler> animationStateHandlers;

	//-----------------------------------------------------------------------------------------
	// Public Methods:
	//-----------------------------------------------------------------------------------------

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) { CallStateMethod(animator, stateInfo, AnimationSubStates.Enter); }

	public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) { CallStateMethod(animator, stateInfo, AnimationSubStates.Exit); }

	//-----------------------------------------------------------------------------------------
	// Private Methods:
	//-----------------------------------------------------------------------------------------

	private void CallStateMethod(Animator animator, AnimatorStateInfo stateInfo, AnimationSubStates animationSubState) {
		if (cache && !cached) {
			animationStateHandlers = animator.GetComponentsInChildren<IAnimationStateHandler>().ToList();
			animationStateHandlers.AddRange(animator.GetComponentsInParent<IAnimationStateHandler>());
			cached = true;
		}

		if (animationStateHandlers == null) return;

		foreach (IAnimationStateHandler animationStateHandler in animationStateHandlers) {
			if (animationStateHandler != null) {
				animationStateHandler.AnimationStateEvent(stateInfo, animationSubState);
			}
		}
	}
}