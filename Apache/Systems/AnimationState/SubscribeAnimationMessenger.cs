﻿using System.Collections.Generic;
using Fayte;
using UnityEngine;
using UnityEngine.Experimental.Director;

public class SubscribeAnimationMessenger : StateMachineBehaviour {
	//-----------------------------------------------------------------------------------------
	// Private Fields:
	//-----------------------------------------------------------------------------------------

	private readonly HashSet<IAnimationStateHandler> animationStateHandlers = new HashSet<IAnimationStateHandler>();

	//-----------------------------------------------------------------------------------------
	// Public Methods:
	//-----------------------------------------------------------------------------------------

	public void RegisterStateHandler(IAnimationStateHandler handler) { animationStateHandlers.Add(handler); }
	public void UnregisterStateHandler(IAnimationStateHandler handler) { animationStateHandlers.Remove(handler); }

	public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) { CallStateMethod(stateInfo, AnimationSubStates.Enter); }

	public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) { CallStateMethod(stateInfo, AnimationSubStates.Exit); }

	public override void OnStateMachineEnter(Animator animator, int stateMachinePathHash, AnimatorControllerPlayable controller) {
		CallStateMachineMethod(stateMachinePathHash, AnimationSubStates.Enter);
	}

	public override void OnStateMachineExit(Animator animator, int stateMachinePathHash, AnimatorControllerPlayable controller) {
		CallStateMachineMethod(stateMachinePathHash, AnimationSubStates.Exit);
	}

	//-----------------------------------------------------------------------------------------
	// Private Methods:
	//-----------------------------------------------------------------------------------------

	private void CallStateMethod(AnimatorStateInfo stateInfo, AnimationSubStates animationSubState) {
		if (animationStateHandlers == null) return;

		foreach (IAnimationStateHandler animationStateHandler in animationStateHandlers) {
			if (animationStateHandler != null) {
				animationStateHandler.AnimationStateEvent(stateInfo, animationSubState);
			}
		}
	}

	private void CallStateMachineMethod(int stateMachinePathHash, AnimationSubStates animationSubState) {
		if (animationStateHandlers == null)
			return;

		foreach (IAnimationStateHandler animationStateHandler in animationStateHandlers) {
			if (animationStateHandler != null) {
				animationStateHandler.AnimationStateMachineEvent(stateMachinePathHash, animationSubState);
			}
		}
	}
}