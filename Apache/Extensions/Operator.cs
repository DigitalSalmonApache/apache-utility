namespace Apache.Utilities {
	//-----------------------------------------------------------------------------------------
	// Type Declarations:
	//-----------------------------------------------------------------------------------------

	public enum Operator {
		// ==
		Equality,

		// !=
		Inequality,

		// +
		Addition,

		// -
		Subtraction,

		// <
		LessThan,

		// >
		GreaterThan,

		// <=
		LessThanOrEqual,

		// >=
		GreaterThanOrEqual
	}
}